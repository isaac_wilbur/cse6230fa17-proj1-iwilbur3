vol_frac = 0.2;
npos = 10000;
pr_vol = 4.0/3.0*pi;

domain_vol = vol_frac/(pr_vol * npos);
domain_vol = 1/domain_vol;

box_width = domain_vol^(1/3);

pos = rand(npos,3).*box_width;

rhs = zeros(npos,1);
rhs(length(rhs)) = 1;

fileID = fopen('volume_frac_20.xyz','w');
fprintf(fileID,'%d\n', npos);
fprintf(fileID,'%f %f\n', 0.0, box_width);
for i = 1:npos
     fprintf(fileID,'%d %f %f %f\n', rhs(i), pos(i,1), pos(i,2), pos(i,3));
end
fclose(fileID);