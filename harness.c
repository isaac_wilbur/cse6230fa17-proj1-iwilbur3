#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include <cse6230rand.h>
#include <tictoc.h>
#include "bd.h"

cse6230nrand_t *rval;

// return number of particles in trajectory file
int traj_read_npos(const char *filename)
{
    int npos;
    FILE *fp = fopen(filename, "r");
    fscanf(fp, "%d\n", &npos);
    assert(npos > 0);
    fclose(fp);
    return npos;
}

// read first frame of trajectory file
int traj_read(const char *filename, char *label, int npos, double *pos, int *types)
{
  int npos_read;
  FILE *fp = fopen(filename, "r");
  assert(fp);

  fscanf(fp, "%d\n", &npos_read);
  fgets(label, LINE_LEN, fp);

  assert(npos == npos_read);

  for (int i=0; i<npos; i++, pos+=3)
    fscanf(fp, "%d %lf %lf %lf\n", &types[i], &pos[0], &pos[1], &pos[2]);

  fclose(fp);

  return 0;
}

// append positions to trajectory file
int traj_write(const char *filename, const char *label, int npos, const double *pos, const int *types)
{
  FILE *fp = fopen(filename, "a");
  assert(fp);

  fprintf(fp, "%d\n", npos);
  fprintf(fp, "%s\n", label);

  for (int i=0; i<npos; i++, pos+=3)
    fprintf(fp, "%d %f %f %f\n", types[i], pos[0], pos[1], pos[2]);

  fclose(fp);

  return 0;
}

int main(int argc, char **argv)
{
  if (argc != 4)
  {
    fprintf(stderr, "usage: bd in.xyz out.xyz num_intervals\n");
    return -1;
  }

  char *input_filename  = argv[1];
  char *output_filename = argv[2];
  int num_intervals = atoi(argv[3]);

  if (access(output_filename, F_OK) != -1)
  {
    printf("Output file already exists: %s\nExiting...\n", output_filename);
    return -1;
  }

  int npos = traj_read_npos(input_filename);
  printf("Number of particles: %d\n", npos);
  printf("Number of intervals to simulate: %d\n", num_intervals);

  double *pos   = (double *) malloc(3*npos*sizeof(double));
  int    *types = (int *)    malloc(  npos*sizeof(int));
  assert(pos);
  assert(types);
 
  // initialize random number stream for each thread
  rval = (cse6230nrand_t*)malloc(sizeof(cse6230nrand_t)*omp_get_max_threads());
  assert(rval);
  #pragma omp parallel
  {
    int s_seed = 0;
    #if defined(_OPENMP)
        s_seed = omp_get_thread_num();
    #endif
    cse6230nrand_seed(s_seed, &(rval[omp_get_thread_num()]));
  }

  char label[LINE_LEN];
  double start_time, box_width;

  traj_read(input_filename, label, npos, pos, types);
  sscanf(label, "%lf %lf", &start_time, &box_width);
  printf("Simulation box width: %f\n", box_width);

  int maxnumpairs;
  {
    /* we are using $a = 1$ in this code */
    double pr_vol = (4./3.) * M_PI;
    double domain_vol = box_width*box_width*box_width;
    double vol_frac = pr_vol * npos / domain_vol;
    double interact_vol = 8 * pr_vol;
    double exp_ints_per = npos * interact_vol / domain_vol;
    printf("With %d particles of radius 1 and a box width of %f, the volume fraction is %g.\n",npos,box_width,vol_frac);
    printf("The interaction volume is %g, so we expect %g interactions per particle, %g overall.\n",interact_vol,exp_ints_per,exp_ints_per * npos / 2.);
    maxnumpairs = npos * ceil(exp_ints_per) * 10.;
    printf("Allocating space for %d interactions\n",maxnumpairs);
  }
  double *dist2 = (double *) malloc(maxnumpairs*sizeof(double));
  int    *pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
  assert(dist2);
  assert(pairs);

  double t;
  TicTocTimer timer = tic();

  // simulate for num_intervals, writing frame after each interval
  for (int interval=1; interval<=num_intervals; interval++)
  {
    int retval;

    retval = bd(npos, pos, box_width, types, &maxnumpairs, &dist2, &pairs);
    if (retval) return retval;
    sprintf(label, "%f %f",
            start_time+interval*INTERVAL_LEN*DELTAT, box_width);
    traj_write(output_filename, label, npos, pos, types);

    if (interval % 100 == 0)
      printf("Done interval: %d\n", interval);
  }
  t = toc(&timer);
  printf("Time: %f for %d intervals\n", t, num_intervals);
  printf("Time per time step: %g\n", t/num_intervals/INTERVAL_LEN);

  free(pairs);
  free(dist2);
  free(types);
  free(pos);
  free(rval);

  return 0;
}
