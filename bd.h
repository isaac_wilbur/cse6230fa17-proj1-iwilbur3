#include <cse6230rand.h>

#define INTERVAL_LEN 1000
#define DELTAT       1e-4
#define LINE_LEN     100

extern cse6230nrand_t *rval;

int bd(int        npos,
       double    *pos,
       double     L,
       const int *types,
       int       *maxnumparis_p,
       double   **dist2_p,
       int      **pairs_p);

/* see description in interactions.c */
int interactions(int           npos,
                 const double *pos,
                 double        L,
                 int           boxdim,
                 double        cutoff2,
                 double       *distances2,
                 int          *pairs,
                 int           maxnumpairs,
                 int          *numpairs_p);
