#!/bin/sh
#SBATCH --job-name=harness_example       # Job name
#SBATCH --time=00:05:00                  # Time limit hrs:min:sec
#SBATCH --nodes=1                        # Just one node, but
#SBATCH --exclusive                      # My node alone
#SBATCH --output=harness_48.out          # Standard output and error log

pwd; hostname; date

make

date
