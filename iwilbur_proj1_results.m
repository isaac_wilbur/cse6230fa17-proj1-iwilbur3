% plot results of parallel code

results = [1 0.0124;2 0.0078;4 0.0046;8 0.0030;16 0.0019;32 0.00146;64 0.0051];

plot(results(:,1),results(:,2),'--','LineWidth',2)
set(gca,'fontsize',18)
set(gca, 'YScale', 'log')
xlabel('Number of Threads');
ylabel('Time/Interval');